# Acknowledgements
This application makes use of the following third party libraries:

## AmpCore

©2014 Akamai Technologies, Inc. All Rights Reserved. Reproduction in whole or in part in any form or medium without express written permission is prohibited. Akamai and the Akamai wave logo are registered trademarks. Other trademarks contained herein are the property of their respective owners.
Generated by CocoaPods - https://cocoapods.org
