//
//  ViewController.swift
//  AkamaiPlayerDemo
//
//  Created by benedict placid on 26/02/19.
//  Copyright © 2019 benedict placid. All rights reserved.
//

import UIKit
import Foundation
import AmpCore

var globalAssetValues : String!

class ViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var videoOneEvent        : UIButton!
    @IBOutlet weak var fieldConfigId        : UITextView!
    @IBOutlet weak var envrmntSegment       : UISegmentedControl!
    @IBOutlet weak var connectingURLLabel   : UILabel!
    
    var configId:String         = ""
    var connectionURL:String    = ""
    let devURL:String           = "https://devplatform.datazoom.io/beacon/v1/"
    let qaURL:String            = "https://stagingplatform.datazoom.io/beacon/v1/"
    let preProductionURL:String = "https://demoplatform.datazoom.io/beacon/v1/"
    let productionURL:String    = "https://platform.datazoom.io/beacon/v1/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fieldConfigId.text = "b8af99a5-473e-43f5-bccd-c4faa8e90457"
        self.fieldConfigId.delegate = self
        self.configId = fieldConfigId.text
        self.connectingURLLabel.text = devURL
        self.connectionURL = devURL
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func validateConfigId(configIdL:String) -> Bool {
        if configIdL.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    
    @IBAction func videoOneEvent(_ sender: Any) {
        if validateConfigId(configIdL: self.configId) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController
            vc?.configId = self.configId
            vc?.connectionURL = self.connectionURL
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.showInvalidConfigIdAlert()
            return
        }
        globalAssetValues = "https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8"//"http://thenewcode.com/assets/videos/aotearoa.mp4"//"http://thenewcode.com/assets/videos/atlantic-light.mp4"//"https://thenewcode.com/assets/videos/atlantic-light.mp4"//"http://thenewcode.com/assets/videos/zsystems-alt.mp4"  //"http://thenewcode.com/assets/videos/glacier.mp4"
    }
    
    func showInvalidConfigIdAlert() {
        let alert = UIAlertController(title: "", message: "Please enter a valid Configuration Id.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.configId = fieldConfigId.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func didChangeEnvironment(_ sender: Any) {
        switch envrmntSegment.selectedSegmentIndex {
        case 0:
            self.connectionURL = self.devURL
            print("Dev selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 1:
            self.connectionURL = self.qaURL
            print("QA selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 2:
            self.connectionURL = self.preProductionURL
            print("Pre Productio selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 3:
            self.connectionURL = self.productionURL
            print("Productio selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}




