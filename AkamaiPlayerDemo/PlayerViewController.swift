//
//  PlayerViewController.swift
//  AkamaiPlayerDemo
//
//  Created by benedict placid on 04/03/19.
//  Copyright © 2019 benedict placid. All rights reserved.
//

import UIKit
import Foundation
import DZAkamaiCollector
import AmpCore

let kLicense = "AwHTKJ0VUatordoIO1mf2V0173gfWZqOIs5lQq2KezLx9rdlT7052zBmHzDYXl+F86pg5Szo89xeHLor+YvI86kjeNepkdfRB7HRX0tSLeS9Zi5Q5O+oRO3LSAJzd6gqhEUGLH4OxBanelzcCBPTy70Oh32/EIONXRAxIJ5TE8rj2IfSo57KTARBV9mJPNYV5sj1hMy4XflnPV/iAVUHrQ84CdWbr/qVgV/41Nnn1sAoKA=="
class PlayerViewController: UIViewController {
    
     var ampPlayer : AmpPlayer!
     var configId : String          = ""
     var connectionURL : String     = ""
    
    var buttonPushMe            : UIButton!


    override func viewDidLoad() { 
        super.viewDidLoad()
        self.ampPlayer = AmpPlayer(parentView: self.view)
        self.ampPlayer.play(url: globalAssetValues)
        self.ampPlayer.logsEnabled = false
        self.ampPlayer.setLicense(kLicense)
        self.configurations()
        self.setUpPushMeButton()

    }
    
    func configurations() {
        DZAkamaiCollector.dzSharedManager.flagForAutomationOnly = false
        DZAkamaiCollector.dzSharedManager.initAkamaiPlayerWith(configID: self.configId, url: self.connectionURL, playerInstance: self.ampPlayer)
        
        //MARK:- Custom Evnets And Metadata
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let DZURL = NSURL(string: self.connectionURL)
            let domain = (DZURL?.host)!
            DZAkamaiCollector.dzSharedManager.customMetaData(["customPlayerName" : "iOS Native Player","customDomain":domain])
            DZAkamaiCollector.dzSharedManager.customEvnets("SDKLoaded", metadata: nil)
        }
    }
    
    
    
    func setUpPushMeButton() {
        self.buttonPushMe = UIButton(frame: CGRect(x: (self.view.frame.width / 2) - 45, y: 50, width: 90, height: 40))
        self.buttonPushMe.layer.cornerRadius = 10.0
        self.buttonPushMe.setTitle("Push Me", for: .normal)
        self.buttonPushMe.setTitleColor(UIColor.white, for: .normal)
        self.buttonPushMe.backgroundColor = UIColor(red: 34/255, green: 218/255, blue: 34/255, alpha: 1.0)
        self.buttonPushMe.addTarget(self, action: #selector(self.buttonPushTouched), for: .touchUpInside)
        self.view.addSubview(self.buttonPushMe)
        self.view.bringSubview(toFront: self.buttonPushMe)
    }
    
    
    @objc func buttonPushTouched() {
         DZAkamaiCollector.dzSharedManager.customEvnets("buttonPush", metadata: nil)
    }

}
